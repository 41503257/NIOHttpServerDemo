package com.luoleo;

import java.io.InputStream;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/*
* 通过非阻塞io模型实现http服务器
* 选择器-通道-缓冲区-客户端
* */
public class HttpServer {
    public static void main(String[] args)  {
        try {
            //选择器
            Selector selector=Selector.open();
            //绑定端口，非阻塞
            ServerSocketChannel ssc=ServerSocketChannel.open();
            ssc.configureBlocking(false);
            InetSocketAddress address=new InetSocketAddress(80);
            ssc.bind(address);
            //将选择器注册
            SelectionKey selectionKey=ssc.register(selector,ssc.validOps(),null);
            //监听端口
            while(true){
                //阻塞直到有访问
                int noOfKeys=selector.select();
                Set selectedKeys= selector.selectedKeys();
                Iterator itr=selectedKeys.iterator();
                System.out.println("有连接进入");
                //遍历访问
                while(itr.hasNext()){
                    SelectionKey key=(SelectionKey) itr.next();
                    if(key.isAcceptable()){
                        SocketChannel client=ssc.accept();
                        client.configureBlocking(false);
                        client.register(selector,SelectionKey.OP_READ);
                        System.out.println("连接已经接收");
                    } else if (key.isReadable()) {
                        SocketChannel client = (SocketChannel) key.channel();
                        ByteBuffer buffer = ByteBuffer.allocate(10240);
                        client.read(buffer);
                        String input = new String(buffer.array()).trim();
                        System.out.println(input);
                        //清空buffer并输出输出数据
                        buffer.clear();
                        buffer.put("HTTP/1.1 200 OK\r\n".getBytes());
                        buffer.put("Content-Type: text/html;charset=UTF-8\r\n".getBytes());
                        buffer.put("Content-Length:40\r\n".getBytes());
                        buffer.put("Server:luoserver/1.1\r\n".getBytes());
                        buffer.put(("Date:"+new Date()+"\r\n").getBytes());
                        buffer.put("\r\n".getBytes());
                        buffer.put("<h1>hello!</h1>\r\n".getBytes());
                        buffer.put("<h3>HTTP服务器!</h3>\r\n".getBytes());
                        buffer.flip();
                        client.write(buffer);
                        client.close();
                    }
                    itr.remove();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
